import_data_tab <-tabPanel("Data Import",
  tags$br(),

  sidebarLayout(

    # Sidebar panel for inputs ----
    sidebarPanel(id="sidebar",

      wellPanel(

        h4(tags$b("UPLOAD MULTIASSAYEXPERIMENT")),
        br(),

        fileInput("mae_file", helpText("Select File:"),
                  multiple = FALSE,
                  accept   = c(".RData"))
      ),

      wellPanel(

        h4(tags$b("CREATE MULTIASSAYEXPERIMENT")),
        br(),

        fileInput("exp_file", helpText("Upload Experiments:"),
                  multiple = TRUE,
                  accept   = c("text/csv", "text/comma-separated-values,text/plain", ".csv", ".rds")),
        br(),

        fileInput("pat_data_file", helpText("Upload Patient Data:"),
                  multiple = TRUE,
                  accept   = c("text/csv", "text/comma-separated-values,text/plain", ".csv"))
      )
    ),

    mainPanel(
      actionButton(inputId = "code_uploadMAE", label = " Show R Code", icon("file-code")),
      # actionButton(inputId = "code_createMAE", label = " Show R Code", icon("file-code")),
      br(),
      
      withSpinner(DT::dataTableOutput(outputId = "exp_table")),

      br(),
      
      fluidRow(
        column(6,
          withSpinner(plotlyOutput("count_exp_patients_hist", height = 'auto', width = 'auto'))
        ),
        column(6,
          withSpinner(plotlyOutput("count_exp_feat_hist", height = 'auto', width = 'auto'))
        )
      ),
      fluidRow(
        column(12,
          withSpinner(imageOutput("upsetR", height = 700, width = 1000))
        )
      )
    )
  )
)





