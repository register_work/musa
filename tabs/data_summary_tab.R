data_summary_tab <- tabPanel("Data Summary", value = "summary_data_panel",
                            tags$br(),
                             
                            sidebarLayout(
                              
                            
                              # Input(s)
                              sidebarPanel(id = "sidebar3",
                                          width = 3,
                                wellPanel(
                                  actionButton("show_ds", "Show Your Data Summary", icon("eye"), style="color: #fff; background-color: #337ab7; border-color: #2e6da4")
                                ),
                                
                                br(),
                                br(),
                                
                                checkboxGroupInput("normalized_checkGroup",
                                                   label = helpText("Select Experiments For Your Downstream Analysis:"),
                                                   choices = NULL, selected = NULL)
                              ),
                              
                              # Output(s)
                              mainPanel(width = 9, 
                                        actionButton(inputId = "code_ds", label = " Show R Code", icon("file-code")),
                                        DT::dataTableOutput("summary_tab"),
                                        #DT::dataTableOutput("test"),
                                        plotOutput("clinical_plots"),
                                        DT::dataTableOutput("clinical_tab")
                                        #DT::dataTableOutput("final_exps")
                                        
                                        #verbatimTextOutput("test"),
                                        #uiOutput("plots")
                                        #withSpinner(plotOutput("PCAs"))
                               )
                            )
)
